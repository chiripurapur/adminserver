package com.tessco.boot.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

import de.codecentric.boot.admin.config.EnableAdminServer;

@EnableAutoConfiguration
@EnableAdminServer
public class AdminServerApp {
	public static void main(String[] args) {
		SpringApplication.run(AdminServerApp.class, args);
	}
}
